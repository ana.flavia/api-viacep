﻿using FluentAssertions.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace API_Viacep
{
    public class Endereco
    {
        public string Cep { get; set; }
        public string Logradouro { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Localidade { get; set; }
        public string Uf { get; set; }
        public string Unidade { get; set; }
        public string EnderecoCompleto { get; set; }
    }

    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public Endereco Endereco { get; set; }
        public MainPage()
        {
            InitializeComponent();

            this.Endereco = new Endereco();
            this.BindingContext = this;
        }

        public async void BuscarEndereco()
        {
            Console.WriteLine("Iniciando consumo da API");

            Uri url = new Uri("https://viacep.com.br/ws/"
                + this.Endereco.Cep + "/json/");

            // call endpoint
            HttpResponseMessage httpResponse =
                await Service.HttpService.GetRequest(url.AbsoluteUri);

            if (httpResponse.IsSuccessStatusCode)
            {
                string stringResponse = httpResponse
                    .Content.ReadAsStringAsync().Result;
                Console.WriteLine("\n====================");
                Console.WriteLine(stringResponse);

                Endereco End = Service.SerializationService
                    .DeserializeObject<Endereco>(stringResponse);

                //this.Endereco = End;
                this.Endereco.EnderecoCompleto =
                    End.Logradouro + ", "
                    + End.Bairro + ", "
                    + End.Localidade + " - "
                    + End.Uf;

                OnPropertyChanged(nameof(this.Endereco));
            }

            ActivityIndicatorBuscar.IsRunning = false;
        }

        private void Button_Buscar_Clicked(object sender, EventArgs e)
        {
            BuscarEndereco();
        }
    }

}